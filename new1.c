#include<stdio.h>

void main(){

    //declaration of variables
    FILE* fptr;
    char sen[100];

    //creates and opens the file in the same location
    fptr = fopen("assignment9.txt","w");
    fprintf(fptr,"UCSC is one of the leading institutes in Sri Lanka for computing studies.");
    fclose(fptr);

    //reopens file in read mode to read and output content to screen
    fptr = fopen("assignment9.txt","r");
    fgets(sen,100,fptr);
    puts(sen);
    fclose(fptr);

    //opens file in append mode to add text
    fptr = fopen("assignment9.txt","a");
    fprintf(fptr,"\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
    fclose(fptr);

   return 0;
}

